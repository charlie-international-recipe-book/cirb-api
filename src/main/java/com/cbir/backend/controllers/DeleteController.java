package com.cbir.backend.controllers;
import com.cbir.backend.repositories.CountryRepository;
import com.cbir.backend.repositories.FoodRepository;
import com.cbir.backend.repositories.IngredientRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "recipiebook/delete")
@CrossOrigin
public class DeleteController {
    private final CountryRepository crepo;
    private final FoodRepository frepo;
    private final IngredientRepository irepo;
    private String yes = "Deletion successful";
    private String no = "Error processing delete request";

    public DeleteController(CountryRepository crepo, FoodRepository frepo, IngredientRepository  irepo){
        this.crepo = crepo;
        this.frepo = frepo;
        this.irepo = irepo;
    } 

    @DeleteMapping("/country/{id}")
    public String deleteCountry(@PathVariable long id){
        System.out.println("delete" + id);
        
        try {
            crepo.deleteById(id);
        } catch (Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no;
        }
        System.out.println("delete" + id);
        return yes;
    }

    @DeleteMapping("/food/{id}")
    public String deleteFood(@PathVariable long id){
        System.out.println("delete" + id);
        
        try {
            frepo.deleteById(id);
        } catch (Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no;
        }
        System.out.println("delete" + id);
        return yes;
    }

    @DeleteMapping("/ingredient/{id}")
    public String deleteIngredient(@PathVariable long id){
        System.out.println("delete" + id);
        
        try {
            irepo.deleteById(id);
        } catch (Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no;
        }
        System.out.println("delete" + id);
        return yes;
    }
}
