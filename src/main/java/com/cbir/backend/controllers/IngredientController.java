package com.cbir.backend.controllers;

import java.util.List;

import com.cbir.backend.models.Ingredient;
import com.cbir.backend.repositories.IngredientRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "recipebook")
@CrossOrigin
public class IngredientController {
    private final IngredientRepository ingredientRepo;

    public IngredientController(IngredientRepository ingredientRepo) {
        this.ingredientRepo = ingredientRepo;
    }

    @GetMapping("/ingredients")
    public List<Ingredient> viewAll(Model model) {
        List<Ingredient> ingredientList = ingredientRepo.findAll();
        model.addAttribute("noFoodsAvailable", ingredientList.size() == 0);
        model.addAttribute("foods", ingredientList);
        return ingredientList;
    }
}
