package com.cbir.backend.controllers;

import java.util.List;

import com.cbir.backend.models.Country;
import com.cbir.backend.repositories.CountryRepository;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "recipebook")
@CrossOrigin
public class CountryController {
    private final CountryRepository countryRepo;

    public CountryController(CountryRepository countryRepo) {
        this.countryRepo = countryRepo;
    }

    @GetMapping("/countries")
    public List<Country> viewAll(Model model) {
        List<Country> countryList = countryRepo.findAll();
        model.addAttribute("moCountriesFound", countryList.size() == 0);
        model.addAttribute("countries", countryList);
        return countryList;
    }
}
