package com.cbir.backend.controllers;

import java.util.List;

import com.cbir.backend.models.Country;
import com.cbir.backend.models.Food;
import com.cbir.backend.models.Ingredient;
import com.cbir.backend.repositories.CountryRepository;
import com.cbir.backend.repositories.FoodRepository;
import com.cbir.backend.repositories.IngredientRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "recipebook")
@CrossOrigin
public class CreateController {
    private final CountryRepository crepo;
    private final FoodRepository frepo;
    private final IngredientRepository irepo;
    // private String yes = "creation successful";
    // private String no = "Error processing create request";
    
    public CreateController(CountryRepository crepo, FoodRepository frepo, IngredientRepository  irepo){
        this.crepo = crepo;
        this.frepo = frepo;
        this.irepo = irepo;
    } 

    @PostMapping("/countries/add-country")
    public List<Country> newCountry(
        @RequestParam(name = "country_likeness") String country_likeness,
        @RequestParam(name = "country_name") String country_name,
        @RequestParam(name = "location") String location) {

    Country countryToBeSaved = new Country();
    countryToBeSaved.setCountry_name(country_name);
    countryToBeSaved.setCountry_likeness(country_likeness);
    countryToBeSaved.setLocation(location);
    crepo.save(countryToBeSaved);
    return crepo.findAll();
    }

    // @GetMapping("/Country/{id}")
    // public String viewCountry(@PathVariable long id, Model model, String oneCountry) {
    //     Country country = crepo.getOne(id);
    //     model.addAttribute("countryId", id);
    //     model.addAttribute("country", country);
    //     return oneCountry;
    // }



    @PostMapping("/foods/add-food")
    public List<Food> newFood(
        @PathVariable long id,
        @RequestParam(name = "food_name") String food_name,
        @RequestParam(name = "ingredient_name") String ingredient_name,
        @RequestParam(name = "location") String location,
        @RequestParam(name = "food_price") String food_price) {

    Food foodToBeSaved = new Food();
    foodToBeSaved.setFood_name(food_name);
    foodToBeSaved.setIngredient_name(ingredient_name);
    foodToBeSaved.setLocation(location);
    foodToBeSaved.setFood_price(food_price);
    frepo.save(foodToBeSaved);
    return frepo.findAll();
    }

    // @GetMapping("/Food/{id}")
    // public String viewFood(@PathVariable long id, Model model, String oneCountry) {
    //     Country country = crepo.getOne(id);
    //     model.addAttribute("countryId", id);
    //     model.addAttribute("country", country);
    //     return oneCountry;
    // }

  
    @PostMapping("/ingredients/add-ingredient")
    public List<Ingredient> newIngredient(
        @PathVariable long id,
        @RequestParam(name = "ingredient_name") String ingredient_name,
        @RequestParam(name = "ingredient_details") String ingredient_details,
        @RequestParam(name = "ingredient_price") Long ingredient_price,
        @RequestParam(name = "location") String location) {

    Ingredient ingredientToBeSaved = new Ingredient();
    ingredientToBeSaved.setIngredient_name(ingredient_name);
    ingredientToBeSaved.setIngredient_details(ingredient_details);
    ingredientToBeSaved.setIngredient_price(ingredient_price);
    ingredientToBeSaved.setLocation(location);
    irepo.save(ingredientToBeSaved);
    return irepo.findAll();
    }
    
}
