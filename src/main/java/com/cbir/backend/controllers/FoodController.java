package com.cbir.backend.controllers;

import java.util.List;

import com.cbir.backend.models.Food;
import com.cbir.backend.repositories.FoodRepository;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "recipebook")
@CrossOrigin
public class FoodController {
    private final FoodRepository foodRepo;

    public FoodController(FoodRepository foodRepo) {
        this.foodRepo = foodRepo;
    }

    @GetMapping("/foods")
    public List<Food> viewAll(Model model) {
        List<Food> foodList = foodRepo.findAll();
        model.addAttribute("noFoodsAvailable", foodList.size() == 0);
        model.addAttribute("foods", foodList);
        return foodList;
    }
}
