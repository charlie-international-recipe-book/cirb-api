package com.cbir.backend.controllers;

import com.cbir.backend.models.Country;
import com.cbir.backend.models.Food;
import com.cbir.backend.models.Ingredient;
import com.cbir.backend.repositories.CountryRepository;
import com.cbir.backend.repositories.FoodRepository;
import com.cbir.backend.repositories.IngredientRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "recipiebook/edit")
@CrossOrigin
public class EditController {
    private final CountryRepository crepo;
    private final FoodRepository frepo;
    private final IngredientRepository irepo;
    private String yes = "Edit successful";
    private String no = "Error processing edit request";
    private String no2 = "Error: ID is invalid";

    public EditController(CountryRepository crepo, FoodRepository frepo, IngredientRepository  irepo){
        this.crepo = crepo;
        this.frepo = frepo;
        this.irepo = irepo;
    } 

    @GetMapping("/country/{id}/n")
    public String getCountryN(@PathVariable long id){
        try{
            return crepo.getOne(id).getCountry_name();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }
    @GetMapping("/country/{id}/x")
    public String getCountryX(@PathVariable long id){
        try{
            return crepo.getOne(id).getCountry_likeness();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }

    @GetMapping("/country/{id}/l")
    public String getCountryL(@PathVariable long id){
        try{
            return crepo.getOne(id).getLocation();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }
    @PostMapping("/country/{id}")
    public String editCountry(@PathVariable long id,
                            @RequestParam(name = "country_likeness") String cl,
                            @RequestParam(name = "country_name") String name,
                            @RequestParam(name = "location") String loc) {
        try{
            Country edit = crepo.getOne(id);
            edit.setCountry_likeness(cl);
            edit.setCountry_name(name);
            edit.setLocation(loc);
            crepo.save(edit); 
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no;
            }
        return yes;
    }

    @GetMapping("/food/{id}/n")
    public String getFoodN(@PathVariable long id){
        try{
            return frepo.getOne(id).getFood_name();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }
    @GetMapping("/food/{id}/p")
    public String getFoodP(@PathVariable long id){
        try{
            return frepo.getOne(id).getFood_price();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }
    @GetMapping("/food/{id}/l")
    public String getFoodL(@PathVariable long id){
        try{
            return frepo.getOne(id).getLocation();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }
    @GetMapping("/food/{id}/i")
    public String getFoodI(@PathVariable long id){
        try{
            return frepo.getOne(id).getIngredient_name();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }

    @PostMapping("/food/{id}")
    public String editFood(@PathVariable long id,
                            @RequestParam(name = "food_name") String name,
                            @RequestParam(name = "food_price") String price,
                            @RequestParam(name = "location") String loc,
                            @RequestParam(name = "ingredient_name") String iname) {
        try{
            Food edit = frepo.getOne(id);
            edit.setFood_name(name);
            edit.setFood_price(price);
            edit.setIngredient_name(iname);
            edit.setLocation(loc);
            frepo.save(edit); 
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no;
            }
        return yes;
    }

    @GetMapping("/ingredients/{id}/n")
    public String getIngN(@PathVariable long id){
        try{
            return irepo.getOne(id).getIngredient_name();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }

    @GetMapping("/ingredients/{id}/p")
    public String getIngP(@PathVariable long id){
        try{
            return String.valueOf(irepo.getOne(id).getIngredient_price());
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }

    @GetMapping("/ingredients/{id}/l")
    public String getIngL(@PathVariable long id){
        try{
            return irepo.getOne(id).getLocation();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }

    @GetMapping("/ingredients/{id}/d")
    public String getIngD(@PathVariable long id){
        try{
            return irepo.getOne(id).getIngredient_details();
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no2;
        }
    }

    @PostMapping("/ingredients/{id}")
    public String editIngredients(@PathVariable long id,
                            @RequestParam(name = "ingredient_details") String ingdet,
                            @RequestParam(name = "ingredient_price") long price,
                            @RequestParam(name = "location") String loc,
                            @RequestParam(name = "ingredient_name") String name) {
        try{
            Ingredient edit = irepo.getOne(id);
            edit.setIngredient_details(ingdet);
            edit.setIngredient_price(price);
            edit.setIngredient_name(name);
            edit.setLocation(loc);
            irepo.save(edit); 
        } catch(Exception bad) {
            System.out.println("\n" + bad.getMessage() +"\n" + bad.getCause() + "\n");
            return no;
            }
        return yes;
    }
}
