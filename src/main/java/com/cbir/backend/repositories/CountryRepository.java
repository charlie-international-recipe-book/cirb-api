package com.cbir.backend.repositories;

import com.cbir.backend.models.Country;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
    
}
