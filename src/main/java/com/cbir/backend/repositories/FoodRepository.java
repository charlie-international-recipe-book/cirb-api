package com.cbir.backend.repositories;

import com.cbir.backend.models.Food;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food, Long>{
    
}
