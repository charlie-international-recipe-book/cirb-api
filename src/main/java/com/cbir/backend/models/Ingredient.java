package com.cbir.backend.models;

import javax.persistence.*;

@Entity
@Table(name = "ingredients")
public class Ingredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 150)
    private String ingredient_name;

    @Column(nullable = false)
    private String ingredient_details;

    @Column(nullable = false)
    private long ingredient_price;

    @Column(nullable = false)
    private String location;

    public Ingredient() {
    }

    public Ingredient(long id, String ingredient_name, String ingredient_details, long ingredient_price,
            String location) {
        this.id = id;
        this.ingredient_name = ingredient_name;
        this.ingredient_details = ingredient_details;
        this.ingredient_price = ingredient_price;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

    public String getIngredient_details() {
        return ingredient_details;
    }

    public void setIngredient_details(String ingredient_details) {
        this.ingredient_details = ingredient_details;
    }

    public long getIngredient_price() {
        return ingredient_price;
    }

    public void setIngredient_price(long ingredient_price) {
        this.ingredient_price = ingredient_price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
