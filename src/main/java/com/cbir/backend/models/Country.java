package com.cbir.backend.models;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 150)
    private String country_name;

    @Column(nullable = false)
    private String location;

    @Column(nullable = false, length = 150)
    private String country_likeness;

    public Country() {
    }

    public Country(long id, String country_name, String location, String country_likeness) {
        this.id = id;
        this.country_name = country_name;
        this.location = location;
        this.country_likeness = country_likeness;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry_likeness() {
        return country_likeness;
    }

    public void setCountry_likeness(String country_likeness) {
        this.country_likeness = country_likeness;
    }

}
